# ANT Build Script

##IT311-9472 Applications Development

##Team Vena Cava
###Ampal, Rob Davy
###Baniaga, Rachel
###Mandapat, Ivan Wrex
###Mangilinan III, Alfredo
###Quia, Angel
###Soriano, Angelica Louise

##Instructions
### 1.) Extract the zip file of this project in a desired location
### 2.) In the command prompt, change directory to the location of the extracted project, then execute the following commands in order:
#### 2.1) ant compile - Compiles the java source codes, creating its classes
#### 2.2) ant doc - Generates the documentation of each source codes
#### 2.3) ant compress - Generates .jar files of the classes and documentations 
