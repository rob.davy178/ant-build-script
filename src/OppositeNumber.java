public class OppositeNumber {
    /**
     * Returns the opposite number of a given number
     * @param number input integer to be converted to its opposite
     * @return opposite of the given number
     */
    public static int opposite(int number) {
        return number *= -1;
    }
}

