public class IsANumberPrime {
    /**
     * This method determines if the inputted number is prime or not
     * @param num This is the parameter
     * @return boolean This returns if the integer is prime or not
     */
    public static boolean isPrime(int num) {
        if (num < 2) {
            return false;
        }
        if (num == 2) {
            return true;
        }
        for (int i = 2; i < Math.sqrt(num) + 1; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
