public class CreatePhoneNumber {
    /**
     * Accepts an array of integers between 1-9
     * And generates a phone number like format
     * For example:
     *      Kata.createPhoneNumber(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0}) // returns "(123) 456-7890"
     * @param numbers an array or numbers
     * @return phoneNumber
     */
    public static String createPhoneNumber(int[] numbers) {
        String phoneNumber = "";
        StringBuilder sb= new StringBuilder();
        sb.append('(');
        sb.append(numbers[0]);
        sb.append(numbers[1]);
        sb.append(numbers[2]);
        sb.append(") ");
        for (int i = 3; i <= 5; i++) {
            sb.append(numbers[i]);
        }
        sb.append('-');
        for (int i = 6; i <= 9; i++) {
            sb.append(numbers[i]);
        }
        phoneNumber = sb.toString();
        return phoneNumber;
    }
}
