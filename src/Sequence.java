public class Sequence {
    /**
     * This method returns an array of integers from n to 1 where n is greater than 0.
     * @param n an integer number input
     * @return int array of integers
     */
    public static int[] reverse(int n){
        int[] reverseNum = new int[n];
        int index=0;
        while(n > 0) {
            reverseNum[index] = n;
            n--;
            index++;
        }
        return reverseNum;
    }
}
