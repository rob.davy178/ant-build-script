public class SquareEveryDigit {
    /**
     * Computes the square of each digit in the given integer and concatenates them.
     * @param n an integer number
     * @return concatenated digits that were multiplied by itself
     */
    public int squareDigits(int n) {
        String num = String.valueOf(n);
        String result = "";
        for(int i=0; i < num.length() ; i++){
            int number = Character.getNumericValue(num.charAt(i)) * Character.getNumericValue(num.charAt(i));
            result = result + number;
        }
        return Integer.parseInt(result);
    }
}
