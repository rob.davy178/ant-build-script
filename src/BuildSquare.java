public class BuildSquare {
    /**
     * This method will build a square that is as long and wide for the integer inputted.
     * @param n This is a parameter
     * @return sb.toString().trim() This will return the square output of the given integer
     */
    public static final String generateShape(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                sb.append("+");
            }
            sb.append("\n");
        }
        return sb.toString().trim();
    }
}
