public class RomanNumeralEncoder {
    /**
     * Returns the roman numeral version of a number
     * @param number input integer to be converted to its roman numeral version
     * @return the roman numeral string of the number
     */
    public String solution(int number) {
        String romanString = "";
        int[] val = {1000, 900, 500, 400, 100, 90, 50, 40, 9, 5, 4, 1};
        String[] roman = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};

        for (int i = 0; i < val.length; i++) {
            while (number >= val[i]) {
                number = number - val[i];
                romanString = romanString + roman[i];
            }
        }
        return romanString;
    }
}