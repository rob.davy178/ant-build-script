
public class SortTheOdd {
    /**
     * Sorts the odd numbers in ascending order a given array while retaining
     * the order of the even numbers.
     * @param array array of integers
     * @return odd numbers sorted in ascending order
     */
    public static int[] sortOdd(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int n = 0; n < array.length; n++) {
                if (array[n] % 2 != 0) {
                    for (int m = n + 1; m < array.length; m++) {
                        if (array[m] % 2 != 0) {
                            if (array[n] > array[m]) {
                                int temp = array[n];
                                array[n] = array[m];
                                array[m] = temp;
                            }
                            break;
                        }
                    }
                }
            }
        }
        return array;
    }
}
