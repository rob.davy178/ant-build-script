public class Snail {
    /**
     * Arrange integers from outermost elements to the middle element while traveling clockwise
     * @param array a multidimensional array with equal length and width
     * @return Am single dimensional array with the proper arrangement of integers
     */
    public static int[] snail(int[][] array) {
        int rows=array.length;
        if(array.length<1||array[0].length<1){
            return new int[0];
        }else {
            return arrayOfOuterSquare(rows, 0, array);
        }
    }
    private static int[] arrayOfOuterSquare(int size, int offset, int[][] twoDArray) {
        int[] array = new int[size * size];
        short index = 0;
        int row = offset;
        int column = offset;
        if (size > 0) {
            if (size == 1) {
                array[index] = twoDArray[row][column];
            } else {
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    column++;
                } while (column < size - 1 + offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    row++;
                } while (row < size - 1 + offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    column--;
                } while (column > offset);
                do {
                    array[index] = twoDArray[row][column];
                    index++;
                    row--;
                } while (row > offset);
                int[] arrayLatter = arrayOfOuterSquare(size - 2, offset + 1, twoDArray);
                for (short i = 0; i < arrayLatter.length; i++) {
                    array[i + index] = arrayLatter[i];
                }
            }
        }
        return array;
    }
}

