public class AreTheNumbersInOrder {
    /**
     * This method determines if the array is in ascending order.
     * @param arr This is an array parameter
     * @return boolean This will return if the array is in ascending or not.
     */
    public static boolean isAscOrder(int[] arr) {
        // TODO
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i + 1] < arr[i]) {
                return false;
            }
        }
        return true;
    }
}
