public class OddEven {
    /**
     * Returns whether a number is "even" or "odd"
     * @param array stores an array of a numbers
     * @return "even" for even numbers or "odd" for odd numbers
     */
    public static String oddOrEven (int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i = i + 1) {
            sum = sum + array[i];
        }

        if (sum % 2 == 0) {
            return "even";
        } else {
            return "odd";
        }
    }
}
