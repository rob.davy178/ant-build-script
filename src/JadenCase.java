public class JadenCase {
    /**
     * In Twitter, Jaden Smith is known for almost always capitalizing every word.
     * This method converts strings to how they would be written by him.
     * @param phrase String words input
     * @return String capitalized words
     */
    public String toJadenCase(String phrase) {
        if (phrase == null || phrase.equals(""))
            return null;

        char[] words = phrase.toCharArray();
        String jadenCase = "";

        for (int index = 0; index < words.length; index++) {
            if (index == 0 || words[index-1] == ' ')
                words[index] = Character.toUpperCase(words[index]);
        }

        return new String(words);
    }
}
