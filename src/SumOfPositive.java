public class SumOfPositive {
    /**
     * Computes the sum of all positive integers in a given array.
     * Returns a sum of 0 if there is nothing to sum.
     * @param arr array of integers
     * @return sum of all positive integers
     */
    public static int sum(int[] arr){
        int l = arr.length;
        int total = 0;
        for(int i = 0; i<l; i++){
            if(arr[i]>0){
                total=total+arr[i];
            }
        }
        return total;
    }
}
