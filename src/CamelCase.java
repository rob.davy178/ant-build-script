import java.lang.StringBuilder;
class CamelCase {
    /**
     * Converts the string into Camel Case
     * String input is delimited by '-' or '_'
     * @param toConvertString a string to be converted
     * @return convertedString
     */
    static String toCamelCase(String toConvertString)	{
        if(toConvertString.length()==0	) {
            return "";
        }
        StringBuilder outputSB = new StringBuilder();
        if(Character.isLetter(toConvertString.charAt(0)))  {
            outputSB.append(toConvertString.charAt(0));
        }
        boolean prevIsDelim=false;
        for(int i=1;i<toConvertString.length();i++) {
            char c=toConvertString.charAt(i);
            if(c=='-'||c=='_') {
                prevIsDelim=true;
                continue;
            }
            if(!Character.isLetter(c)) {
                continue;
            }
            if(prevIsDelim) {
                outputSB.append(Character.toUpperCase(c));
            } else {
                outputSB.append(Character.toLowerCase(c));
            }
            prevIsDelim=false;
        }

        String convertedString =  outputSB.toString();
        return convertedString;
    }
}
