public class MexicanWave {
    /**
     * Just like a mexican wave, this returns a string in an array
     * where each succeeding letter should be an uppercase letter
     * @param str input string composed of the letters
     * @return an array of strings that exhibits the mexican wave
     */
    public static String[] wave (String str){
        int i=0;
        int b=0;
        int arrLength = str.length();

        for(int c=0; c<str.length(); c++){
            if(str.charAt(c) == ' '){
                arrLength--;
            }
        }

        String[] fin = new String[arrLength];

        for(i = 0; i <arrLength; i++){
            String result = "";
            int check = i;
            for(b = 0; b < str.length(); b++){
                if (str.charAt(b) == ' '){
                    check++;
                    result = result + "";
                }

                if (check == b) {
                    result = result +
                            Character.toUpperCase(str.charAt(b));
                } else {
                    result = result + str.charAt(b);
                }
            }
            fin[i] = result;
        }
        return fin;
    }
}
