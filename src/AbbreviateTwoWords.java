public class AbbreviateTwoWords {
    /**
     * This method converts a two-word name into two-capital letter initials
     * with a period separating them.
     * @param name two-word name input
     * @return String name initials
     */
    public static String abbrevName(String name) {
        String[] initial = name.split(" ");
        name = initial[0].toUpperCase().charAt(0) + "." + initial[1].toUpperCase().charAt(0);

        return name;
    }
}
