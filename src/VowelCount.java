public class VowelCount {
    /**
     * Returns the number of vowels in a given string/word.
     * @param str input string composed of lower case letters and/or spaces
     * @return vowel count of the input
     */
    public static int getCount(String str) {
        int c = str.length();
        int result = 0;
        for(int i = 0; i<c; i++){
            char chr = str.charAt(i);
            if(chr=='a' || chr=='e' || chr=='i' || chr=='o' || chr=='u'){
                result++;
            }
        }
        return result;
    }
}
