public class DoubleChar {
    /**
     * This method will double each character of the inputted string.
     * @param s This is a parameter
     * @return b This will return the string that each character has been repeated.
     */
    public static String doubleChar(String s){
        String b = "";
        for(int i = 0; i < s.length(); i++){
            b += s.charAt(i);
            b += s.charAt(i);
        }
        return b;
    }
}
