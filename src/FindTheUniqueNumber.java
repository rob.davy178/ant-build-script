public class FindTheUniqueNumber {
    /**
     * Finds the unique integer within the given array
     * It’s guaranteed that array contains at least 3 numbers.
     * @param integerArray an array of integers
     * @return unique
     */
    public static double findUniq (double integerArray[]){
        double unique = 0;
        double common = 0;
        if(integerArray[0] == integerArray[1]){
            common = integerArray[0];
        }else {
            common = integerArray[2];
        }
        for(int i = 0; i < integerArray.length; i++){
            if(common!=integerArray[i]){
                unique = integerArray[i];
            }
        }
        return unique;
    }
}
