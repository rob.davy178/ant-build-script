public class SplitString {
    /**
     * Splits a given string into pairs of two characters.
     * The last character of the last pair is replaced with an underscore (_) if the string
     * has an odd number of characters.
     * @param s a word, combination of letter/s, or phrase
     * @return letters
     */
    public static String[] solution(String s) {
        //Write your code here
        int arraySize = 2;
        if (s.length()%2!=0){
            s+="_";
        }
        String[] letters = s.split("(?<=\\G.{" + arraySize + "})");
        return letters;
    }
}
