public class Solution {
    /**
     * This method receives an array of integers and
     * determines whether the numbers are in ascending order.
     * @param arr array of integers input
     * @return boolean true or false
     */
    public static boolean isAscOrder(int[] arr) {
        boolean ordered = false;

        for (int i = 0; i < arr.length-1; i++) {
            if(arr[i] > arr[i+1]) {
                return ordered;
            }
        }
        return ordered = true;
    }
}
