import java.util.HashSet;
public class CountingDuplicates {
    /**
     * Counts the number of character that appeared more than once in a given text
     * The string is assumed to contain only alphabet and numeric digits
     * Lowercase and Uppercase alphabets are the same
     * @param text a set of character or string that contains duplicate alphabet or numerics
     * @return repeatedCharacters
     */
    public static int duplicateCount(String text) {
        int repeatedCharacters = 0;
        HashSet<Character> charSet = new HashSet();
        HashSet<Character> repeatedCharSet = new HashSet();
        for(int i=0;i<text.length();i++){
            char currChar=Character.toLowerCase(text.charAt(i));
            if(charSet.contains(currChar)){
                repeatedCharSet.add(currChar);
            }
            charSet.add(currChar);
        }
        repeatedCharacters = repeatedCharSet.size();
        return repeatedCharacters;
    }
}
